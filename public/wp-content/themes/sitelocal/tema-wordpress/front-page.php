<?php  
    get_header();
?>
<main id="homepage">     
        <section id="sct-1">
            <h1><?php the_field('titulo') ?></h1>
            <p><?php the_field('titulo_desc') ?></p>  
            <img src="<?php the_field('bg-grande') ?>" alt="">      
        </section>
        <section id="sct-2" class="container">
            <div class="card">
                <div><h2>Missão</h2></div>
                <p><?php the_field('missao') ?></p>
            </div>
            <div class="card">
                <div><h2>Visão</h2></div>
                <p><?php the_field('missao') ?></p>
            </div>
        </section>
        <section id="sct-3" class="container">
            <h2>Nossos Serviços</h2>
            <div class="container">
                <div class="card"><h3><?php the_field('primeiro_servico') ?></h3></div>
                <div class="card"><h3><?php the_field('segundo_servico') ?></h3></div>
                <div class="card"><h3><?php the_field('terceiro_servico') ?></h3></div>
            </div>
            <a href="/servicos.html">Saiba mais</a>
        </section>
    </main>
    <?php
        get_footer()
    ?>