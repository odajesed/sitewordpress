<?php 
    function add_styles_and_scripts(){

        if(!is_single()) {
            wp_enqueue_style( "reset-sheet", get_template_directory_uri(). "/css/reset.css" );
        }
       
        wp_enqueue_style( "style-sheet", get_template_directory_uri(). "/style.css" );
        wp_enqueue_script("mail-go_script", get_template_directory_uri(). "/assets/js/mailgo.min.js");

    }
    add_action( 'wp_enqueue_scripts','add_styles_and_scripts');


    function create_custom_post_news() {
        
 
        // Set the labels, this variable is used in the $args array
        $labels = array(
          'name'               => __( 'Notícias' ),
          'singular_name'      => __( 'Notícia' ),
          'add_new'            => __( 'Adicionar nova notícia' ),
          'add_new_item'       => __( 'Adicionar nova notícia' ),
          'edit_item'          => __( 'Editar notícia' ),
          'new_item'           => __( 'Nova notícia' ),
          'all_items'          => __( 'Todas notícias' ),
          'view_item'          => __( 'Ver notícias' ),
          'search_items'       => __( 'Procurar notícias' ),
          'featured_image'     => 'Poster',
          'set_featured_image' => 'Add Poster'
        );
       
        // The arguments for our post type, to be entered as parameter 2 of register_post_type()
        $args = array(
          'labels'            => $labels,
          'description'       => 'Noticias do site do Albuquerque',
          'public'            => true,
          'menu_position'     => 5,
          'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'custom-fields' ),
          'has_archive'       => true,
          'show_in_admin_bar' => true,
          'show_in_nav_menus' => true,
          'has_archive'       => true,
          'query_var'         => 'noticias'
        );
        // Call the actual WordPress function
        // Parameter 1 is a name for the post type
        // Parameter 2 is the $args array
        register_post_type( 'noticias', $args);
        unregister_post_type( 'noticia' );
        unregister_post_type( 'news' );
        unregister_post_type( 'movie' );
    }
    add_action( 'init', 'create_custom_post_news' )
        
?>