<?php get_header();
//Template Name: Noticias
         $args = array(
            'posts_per_page' => 4,
            'post_type'=>'noticias',
            'post_status'=>'publish',
            'suppress_filters'=> true,
            'orderby'=>'post_date',
            'order'=>'DESC'
        );
        $the_query = new WP_Query($args);
        wp_reset_postdata();
?>

<main id="noticias">
        <section id="sct-2">
            <div class="container">
                <h1>Últimas Notícias</h1>
                <div class="last-news">
                <?php if ($the_query->have_posts() ) : 
                    while($the_query->have_posts() ) :
                        $the_query->the_post(); ?>
                        <div class="card">
                            <h5><?php the_title() ?></h5>
                            <p><?php echo wp_strip_all_tags(the_content() ) ?></p>
                            <a href="<?php permalink_link( ) ?>">Continuar lendo</a>
                        </div>
                   
                    <?php  endwhile;
                else: ?>
                    <p><?php _e('Desculpe, não há publicações de acordo com seu critério!') ?></p>
                    <?php endif; ?>

            </div>
                <a href="/todas-noticias">Ver Todas</a>
                <p style="display: none;">Não há notícias no momento!</p>
                <div class="card links-uteis">
                    <h4>Links Úteis</h4>
                    <ul>
                        <li><a href="#">Link para site</a></li>
                        <li><a href="#">Link para site</a></li>
                        <li><a href="#">Link para site</a></li>
                        <li><a href="#">Link para site</a></li>
                    </ul>
                </div>
            </div>
        </section>
    </main>
<?php get_footer() ?>