<?php get_header();
//Template Name: Todas Noticias
    $paged = (get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    

    $args = array(
        'posts_per_page' => 6,
        'post_type'=>'noticias',
        'paged' =>$paged,
        'post_status'=>'publish',
        'suppress_filters'=> true,
        'orderby'=>'post_date',
        'order'=>'DESC'
    );
    $news = new WP_Query($args);
            wp_reset_postdata();

?>

<main id="all-noticias" class="container">
        <h1>Todas as Notícias</h1>
        <!-- <p>Na há notícias disponíveis!</p> -->
        <div class="news">
        <?php if ($news->have_posts() ) : 
                    while($news->have_posts() ) :
                        $news->the_post(); ?>
                        <div class="card">
                            <h5><?php the_title() ?></h5>
                            <p><?php echo wp_strip_all_tags(the_content() ) ?></p>
                            <a href="<?php permalink_link( ) ?>">Continuar lendo</a>
                        </div>
                   
                    <?php  endwhile;
                else: ?>
                    <p><?php _e('Desculpe, não há publicações de acordo com seu critério!') ?></p>
                    <?php endif; ?>

        </div>
        <div class="paginate-links">
        <?php $big = 999999999;
            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', get_pagenum_link($big) ),
                'format' =>'?paged=%#%',
                'current' => max(1, get_query_var('paged')),
                'prev_text' => __('Anterior'),
                'next_text' => __('Proximo'),
                'total' => $news->max_num_pages)
            );
            
        
        wp_reset_postdata(  ) ?>
        </div>
    </main>

    <?php get_footer() ?>