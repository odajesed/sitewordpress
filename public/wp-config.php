<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4ANfXfD4uEflEq8fFxMIdw7O6EwGKbWELkirarWPDRD9+S4KJ8/vah6l0Ob0YzCLuVr3hSQtNHGLFHLX1QJOBw==');
define('SECURE_AUTH_KEY',  'oZfEIz3DI23MAJJxlHXY6yLrjm9ceMkcU72cRtLH+/w996Tjy6jp9tn/CQrJdoYwsKWADWnV6D5WjBmk9i8V3Q==');
define('LOGGED_IN_KEY',    'E3qS40PhruQ/8jUT9SkU+Y4EvduuMDr8HCqIxtVg6xW/dBsU9e+wRBDJRr+E/AZyzTrsJTqyhrXQZrYdy6FaFw==');
define('NONCE_KEY',        'qprWBkVFXD2YJeGAK+a/UsfwklUio/daE9dW5Fx1j0Hl8ArlWS4CRSmHERD9JR3vEXUOPrkl3v/xyuMFlRdo8w==');
define('AUTH_SALT',        'N+6GzONBF9Ysn7oYzqPKD/4FlbIECq/SOnvK9iIaUNCgcQO/P0fyMEQan2sQezTECQXXlibvq22e9z5LGZ5Rrw==');
define('SECURE_AUTH_SALT', 'AI28cBcUms/XaW7WPLnnWwbgzNspoA+cFWWMF/TJDMOfBrRXVV1Hwt6b7chK4EWULsem8VTXXIa5N9oA+fOi/g==');
define('LOGGED_IN_SALT',   '6WQFx2bZKxFRoTxMJ0hqAUM88yf05BChSc2grZbaaY6NlU/LRJg8KhEXJwZMnV4AR8q3W658kI1r4pqf1eCLEQ==');
define('NONCE_SALT',       'BIHLdOumx/YGf9r74V1j1Lom55ExCD5BeteMwYE/hAWzk/du9e5kElB2yHQzpsVxrShGbEHBowR9GMyLgVQjcg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
